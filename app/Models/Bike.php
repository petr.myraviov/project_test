<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bike extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_group',
        'type',
        'brand',
        'model',
        'EAN',
        'price',
        'gender',
        'colour',
        'number',
        'size',
        'year',
        'status',
        'promotional_price',
    ];

}
