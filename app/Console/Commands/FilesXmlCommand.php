<?php

namespace App\Console\Commands;

use App\Exports\FilesXmlExport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail as MailAlias;
use Maatwebsite\Excel\Facades\Excel;

class FilesXmlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:xml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $files =  Excel::raw(new FilesXmlExport,\Maatwebsite\Excel\Excel::XLSX, 'filesXlm.xlsx','public');
        MailAlias::send(['filesXml.xlsx' => 'mail'], ['name' =>'Subject'], function ($massage) use ($files){
            $massage->to('test.testing.26.10.1987@gmail.com', 'filesXml.xlsx')->subject('Выгрузка');
            $massage->from('test.testing.26.10.1987@gmail.com',"Вот для вас файл")->attachData($files,'filesXml.xlsx');
        });
    }
}
