<?php

namespace App\Console\Commands;

use App\Exports\TestDataRecordExport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail as MailAlias;
use Maatwebsite\Excel\Facades\Excel;

class TestDataRecordParse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $files =  Excel::raw(new TestDataRecordExport,\Maatwebsite\Excel\Excel::XLSX, 'files.xlsx','public');
        MailAlias::send(['files.xlsx' => 'mail'], ['name' =>'Subject'], function ($massage) use ($files){
            $massage->to('test.testing.26.10.1987@gmail.com', 'files.xlsx')->subject('Выгрузка');
            $massage->from('test.testing.26.10.1987@gmail.com',"Вот для вас файл")->attachData($files,'files.xlsx');
        });

    }
}
