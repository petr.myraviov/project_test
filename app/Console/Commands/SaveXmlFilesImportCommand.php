<?php

namespace App\Console\Commands;

use App\Imports\UploadFilesImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Excel;

//use Maatwebsite\Excel\Facades\Excel;

class SaveXmlFilesImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'saveXml:FileXmlImport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        (new UploadFilesImport)->import('storage/app/public/files.xml', \Maatwebsite\Excel\Excel::XML);

    }
}
