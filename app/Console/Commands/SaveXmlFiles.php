<?php

namespace App\Console\Commands;

use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Console\Command;
use function Composer\Autoload\includeFile;

class SaveXmlFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'saveFiles:FileXml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param Request $request
     * @return void
     */
    public function handle(Request $request)
    {
        error_reporting(E_ALL);
        ini_set('display_errors ', 'on');

        if ($xml = file_exists('storage/app/public/files.xml') == false){

            echo 'Error there are no files in the directory'."\n";
        }else{

        $xml = simplexml_load_file('storage/app/public/files.xml');

        $files = File::all();

            if ($files->isEmpty()) {

                foreach ($xml as $value) {

                    $files_xml = new File($request->all());

                    $files_xml['import'] = intval($value->Import);
                    $files_xml['number'] = intval($value->VCNummer);
                    $files_xml['category'] = intval($value->Kategorie);
                    $files_xml['category_unter'] = intval($value->Unterkategorie);
                    $files_xml['mountain_bike'] = intval($value->Mountainbike);
                    $files_xml['stock'] = intval($value->LagerNr);
                    $files_xml['create_at'] = strval($value->ErfassungsDatum);
                    $files_xml['update_at'] = strval($value->MutationsDatum);
                    $files_xml['title'] = ucfirst($value->Titel);
                    $files_xml['speed'] = intval($value->Geschwindigkeit);
                    $files_xml['brand'] = strtoupper($value->Marke);
                    $files_xml['model'] = strtoupper($value->Modell);
                    $files_xml['suspension'] = intval($value->Federung);
                    $files_xml['frame_style'] = intval($value->Rahmenart);
                    $files_xml['wheel_size'] = intval($value->Radgroesse);
                    $files_xml['frame_material'] = intval($value->Rahmenmaterial);
                    $files_xml['frame_size'] = intval($value->Rahmengroesse);
                    $files_xml['frame_type'] = intval($value->Rahmentyp);
                    $files_xml['frame_size_2'] = intval($value->Rahmengroesse2);
                    $files_xml['drive'] = intval($value->Antrieb);
                    $files_xml['drive_text'] = strtoupper($value->Antriebtext);
                    $files_xml['drive_info'] = strtoupper($value->Antriebinfo);
                    $files_xml['colour'] = intval($value->Farbe);
                    $files_xml['color_text'] = strtoupper($value->Farbetext);
                    $files_xml['engine'] = intval($value->Motor);
                    $files_xml['engine_info'] = strtoupper($value->Motorinfo);
                    $files_xml['model_year'] = intval($value->Modelljahr);
                    $files_xml['status'] = intval($value->Zustand);
                    $files_xml['warranty_period'] = ucfirst($value->Garantiezeit);
                    $files_xml['selling_price'] = intval($value->Verkpreis);
                    $files_xml['rate_code'] = ucfirst($value->Preistyp);
                    $files_xml['factory_price'] = intval($value->Neupreis);
                    $files_xml['description'] = strval($value->Beschreibung);
                    $files_xml['photo'] = strtolower($value->Fotolink);
                    $files_xml['photo_2'] = strtolower($value->Fotolink2);
                    $files_xml['photo_3'] = strtolower($value->Fotolink3);
                    $files_xml['title_4'] = strtolower($value->Fotolink4);
                    $files_xml['photo_5'] = strtolower($value->Fotolink5);
                    $files_xml['active'] = intval($value->Aktiv);

                    $files_xml->save();
                }
            } else {

                foreach ($files as $file) {
                    foreach ($xml as $value) {

                        if ($file->stock === intval($value->LagerNr)) {
                            $file['import'] = intval($value->Import);
                            $file['number'] = intval($value->VCNummer);
                            $file['category'] = intval($value->Kategorie);
                            $file['category_unter'] = intval($value->Unterkategorie);
                            $file['mountain_bike'] = intval($value->Mountainbike);
                            $file['stock'] = intval($value->LagerNr);
                            $file['create_at'] = strval($value->ErfassungsDatum);
                            $file['update_at'] = strval($value->MutationsDatum);
                            $file['title'] = ucfirst($value->Titel);
                            $file['speed'] = intval($value->Geschwindigkeit);
                            $file['brand'] = strtoupper($value->Marke);
                            $file['model'] = strtoupper($value->Modell);
                            $file['suspension'] = intval($value->Federung);
                            $file['frame_style'] = intval($value->Rahmenart);
                            $file['wheel_size'] = intval($value->Radgroesse);
                            $file['frame_material'] = intval($value->Rahmenmaterial);
                            $file['frame_size'] = intval($value->Rahmengroesse);
                            $file['frame_type'] = intval($value->Rahmentyp);
                            $file['frame_size_2'] = intval($value->Rahmengroesse2);
                            $file['drive'] = intval($value->Antrieb);
                            $file['drive_text'] = strtoupper($value->Antriebtext);
                            $file['colour'] = intval($value->Farbe);
                            $file['color_text'] = strtoupper($value->Farbetext);
                            $file['engine'] = intval($value->Motor);
                            $file['engine_info'] = strtoupper($value->Motorinfo);
                            $file['model_year'] = intval($value->Modelljahr);
                            $file['status'] = intval($value->Zustand);
                            $file['warranty_period'] = ucfirst($value->Garantiezeit);
                            $file['selling_price'] = intval($value->Verkpreis);
                            $file['rate_code'] = ucfirst($value->Titel);
                            $file['factory_price'] = intval($value->Neupreis);
                            $file['description'] = strval($value->Beschreibung);
                            $file['photo'] = strtolower($value->Fotolink);
                            $file['photo_2'] = strtolower($value->Fotolink2);
                            $file['photo_3'] = strtolower($value->Fotolink3);
                            $file['title_4'] = strtolower($value->Fotolink4);
                            $file['photo_5'] = strtolower($value->Fotolink5);
                            $file['active'] = intval($value->Aktiv);

                            $file->update();
                        }
                    }
                }
            }
        }
    }
}
