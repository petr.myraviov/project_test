<?php

namespace App\Console\Commands;

use App\Imports\FilesCsvImport;
use App\Models\File;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class SaveFilesCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'saveFiles:FileCsv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param Request $request
     * @return void
     */
    public function handle(Request $request)
    {
        if ($xml = file_exists('storage/app/public/Velocorner 20210104 - PS Cycling AG.csv') == false) {

            echo 'Error there are no files in the directory' . "\n";

        } else {

            $reade_files = file("storage/app/public/Velocorner 20210104 - PS Cycling AG.csv");

            $files = File::all();

            if ($files->isEmpty()) {

                foreach ($reade_files as $key => $value) {

                    $arrays_files_csv = explode(";", ($value));
                    $value_csv_files = str_replace('"', false, $arrays_files_csv);

                    $files_csv = new File($request->all());

                    ($files_csv['product_group'] = ($value_csv_files[0]));
                    ($files_csv['type'] = $value_csv_files[1]);
                    $files_csv['brand'] = $value_csv_files[2];
                    $files_csv['model'] = $value_csv_files[3];
                    $files_csv['RRP'] = intval($value_csv_files[4]);
                    $files_csv['colour'] = $value_csv_files[5];
                    $files_csv['gender'] = $value_csv_files[6];
                    $files_csv['quantity'] = $value_csv_files[7];
                    $files_csv['size'] = intval($value_csv_files[9]);
                    $files_csv['year'] = $value_csv_files[10];
                    $files_csv['condition'] = $value_csv_files[11];
                    $files_csv['special_price'] = intval($value_csv_files[12]);
                    $files_csv['stock'] = intval($value_csv_files[8]);

                    if (intval($value_csv_files[8]) == false) {

                        $files_csv['stock'] = null;

                    } else

                        ($files_csv->save());
                }
            }else{

                foreach ($files as $file) {
                    foreach ($reade_files as $key => $value) {

                        $arrays_files_csv = explode(";", ($value));
                        $value_csv_files = str_replace('"', '', $arrays_files_csv);

                        if ($file->stock === intval($value_csv_files[8])) {

                            $file['product_group'] = ($value_csv_files[0]);
                            $file['type'] = $value_csv_files[1];
                            $file['brand'] = $value_csv_files[2];
                            $file['model'] = $value_csv_files[3];
                            $file['RRP'] = intval($value_csv_files[4]);
                            $file['colour'] = $value_csv_files[5];
                            $file['gender'] = $value_csv_files[6];
                            $file['quantity'] = $value_csv_files[7];
                            $file['stock'] = intval($value_csv_files[8]);
                            $file['size'] = intval($value_csv_files[9]);
                            $file['year'] = $value_csv_files[10];
                            $file['condition'] = $value_csv_files[11];
                            $file['special_price'] = intval($value_csv_files[12]);

                            $file->update();
                        }
                    }
                }
            }
        }
    }
}

