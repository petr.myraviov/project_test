<?php

namespace App\Console\Commands;

use App\Http\Controllers\BikeController;
use App\Imports\BikeImport;
use App\Models\Bike;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use function Composer\Autoload\includeFile;

class BikeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:bikes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (file_exists('storage/app/public/Velocorner 20210104 - PS Cycling AG.csv') == false) {

            echo 'Error there are no files in the directory' . "\n";

        } else

            (new BikeImport)->import('storage/app/public/Velocorner 20210104 - PS Cycling AG.csv', null, \Maatwebsite\Excel\Excel::CSV);

            if (file_exists('storage/app/public/Velocorner Export 20201225 - Radsport Thalmann AG.csv') == false){
                echo 'Error there are no files in the directory' . "\n";

            }else
                (new BikeImport)->import('storage/app/public/Velocorner Export 20201225 - Radsport Thalmann AG.csv', null, \Maatwebsite\Excel\Excel::CSV);

            if (file_exists('storage/app/public/Velocorner_20210107_-_PS_Cycling_AG.csv') == false){

                echo 'Error there are no files in the directory' . "\n";

            }else

                (new BikeImport)->import('storage/app/public/Velocorner_20210107_-_PS_Cycling_AG.csv', null, \Maatwebsite\Excel\Excel::CSV);

        if (file_exists('storage/app/public/Velocorner_Export_20210107_-_Bike_Base_Store_GmbH.csv') == false){

            echo 'Error there are no files in the directory' . "\n";

        }else

            (new BikeImport)->import('storage/app/public/Velocorner_Export_20210107_-_Bike_Base_Store_GmbH.csv', null, \Maatwebsite\Excel\Excel::CSV);

        if (file_exists('storage/app/public/Velocorner_Export_20210107_-_Radsport_Thalmann_AG.csv') == false){

            echo 'Error there are no files in the directory' . "\n";

        }else

            (new BikeImport)->import('storage/app/public/Velocorner_Export_20210107_-_Radsport_Thalmann_AG.csv', null, \Maatwebsite\Excel\Excel::CSV);

    }

}
