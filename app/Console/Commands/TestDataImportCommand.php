<?php

namespace App\Console\Commands;

use App\Imports\TestDataRecordImport;
use App\Models\TestDataRecord;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class TestDataImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:excl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
         Excel::import(new TestDataRecordImport, 'public/files.xlsx');

    }
}
