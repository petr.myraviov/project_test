<?php

namespace App\Imports;

use App\Models\Bike;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithUpserts;

class BikeImport implements ToModel, WithUpserts, WithCustomCsvSettings
{
    use Importable;

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }

    /**
     * @param array $row
     * @return Bike
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $bikes = Bike::all();

        if (!$bikes->isEmpty()) {

            foreach ($bikes as $bike) {

                if ($bike->EAN === $row[8]) {

                    $bike['product_group'] = $row[0];
                    $bike['type'] = $row[1];
                    $bike['brand'] = $row[2];
                    $bike['model'] = $row[3];
                    $bike['price'] = floatval($row[4]);
                    $bike['colour'] = $row[5];
                    $bike['gender'] = $row[6];
                    $bike['number'] = $row[7];
                    $bike['EAN'] = intval($row[8]);
                    $bike['size'] = $row[9];
                    $bike['year'] = $row[10];
                    $bike['status'] = $row[11];
                    $bike['promotional_price'] = floatval($row[12]);

                    Log::info('The bikes database update was successful from the csv file  ' . "| " . $bike. " |" . "\n");

                    $bike->update();

                    return;

                } elseif (is_null($row[8]) && $bike->model === $row[3]) {
                    $bike['product_group'] = $row[0];
                    $bike['type'] = $row[1];
                    $bike['brand'] = $row[2];
                    $bike['model'] = $row[3];
                    $bike['price'] = floatval($row[4]);
                    $bike['colour'] = $row[5];
                    $bike['gender'] = $row[6];
                    $bike['number'] = $row[7];
                    $bike['EAN'] = intval($row[8]);
                    $bike['size'] = $row[9];
                    $bike['year'] = $row[10];
                    $bike['status'] = $row[11];
                    $bike['promotional_price'] = floatval($row[12]);

                    Log::info('The bikes database update was successful from the csv file  ' . "| " . $bike . " |" . "\n");

                    $bike->update();

                    return;

                }elseif (is_null($row[8]) === 0 && $bike->type === $row[1]) {

                    $bike['product_group'] = $row[0];
                    $bike['type'] = $row[1];
                    $bike['brand'] = $row[2];
                    $bike['model'] = $row[3];
                    $bike['price'] = floatval($row[4]);
                    $bike['colour'] = $row[5];
                    $bike['gender'] = $row[6];
                    $bike['number'] = $row[7];
                    $bike['EAN'] = intval($row[8]);
                    $bike['size'] = $row[9];
                    $bike['year'] = $row[10];
                    $bike['status'] = $row[11];
                    $bike['promotional_price'] = floatval($row[12]);

                    Log::info('The bikes database update was successful from the csv file  ' . "| " . $bike. " |" . "\n");

                    $bike->update();

                    return;

                }elseif ($row[8] === ' ' && $bike->type === $row[1] && $bike->price === $row[4]) {

                    $bike['product_group'] = $row[0];
                    $bike['type'] = $row[1];
                    $bike['brand'] = $row[2];
                    $bike['model'] = $row[3];
                    $bike['price'] = floatval($row[4]);
                    $bike['colour'] = $row[5];
                    $bike['gender'] = $row[6];
                    $bike['number'] = $row[7];
                    $bike['EAN'] = intval($row[8]);
                    $bike['size'] = $row[9];
                    $bike['year'] = $row[10];
                    $bike['status'] = $row[11];
                    $bike['promotional_price'] = floatval($row[12]);

                    Log::info('The bikes database update was successful from the csv file  ' . "| " . $bike. " |" . "\n");

                    $bike->update();

                    return;
                }
            }
        }

        if (($row[0] == 'Warengruppe')) {

            return null;

        }

        if ($bikes && $row[8] !== 0) {

                 $bikes_new = new Bike([
                    'product_group' => $row[0],
                    'type' => $row[1],
                    'brand' => $row[2],
                    'model' => $row[3],
                    'price' => floatval($row[4]),
                    'colour' => strval($row[5]),
                    'gender' => $row[6],
                    'number' => $row[7],
                    'EAN' => intval($row[8]),
                    'size' => $row[9],
                    'status' => $row[10],
                    'year' => $row[11],
                    'promotional_price' => floatval($row[12]),
                ]);

                Log::info('Loading data from csv file was successful. ' . "| " . $bikes_new. " |" . "\n");

                return $bikes_new;
            }
    }
    /**
     * @return string|array
     */
    public function uniqueBy()
    {
        return ['EAN', 'model','type', 'price'];
    }

    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ";"
        ];
    }
}
