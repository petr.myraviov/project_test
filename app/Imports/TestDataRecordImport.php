<?php

namespace App\Imports;

use App\Models\TestDataRecord;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;

class TestDataRecordImport implements ToModel
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new TestDataRecord([
            'title' => $row[1],
            'description' => $row[2],
        ]);
    }
}
