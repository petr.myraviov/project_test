<?php

namespace App\Http\Controllers;

use App\Exports\TestDataRecordExport;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class MailController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        $files = Excel::download(new TestDataRecordExport, 'files.xlsx');
        return redirect(route('admin.files.index',compact('files')))->with('status', 'The file was sent to the post office!');
    }

    /**
     * @return bool
     */
    public function storeExcel()
    {
          $files =  Excel::raw(new TestDataRecordExport,\Maatwebsite\Excel\Excel::XLSX, 'files.xlsx','public');
        return $files;

    }

    /**
     *
     */
    public function send()
    {
        $files = $this->storeExcel();
            Mail::send(['files.xlsx' => 'mail'], ['name' =>'Subject'], function ($massage) use ($files){
            $massage->to('test.testing.26.10.1987@gmail.com', 'files.xlsx')->subject('Выгрузка');
            $massage->from('test.testing.26.10.1987@gmail.com',"Вот для вас файл")->attachData($files,'files.xlsx');
        });
        return redirect('admin/files')->with('status', 'The file was sent to the post office!');
    }

    /**
     *
     */
    public function fiesXml()
    {
        $files = $this->storeExcel();
        Mail::send(['filesXml.xlsx' => 'mail'], ['name' =>'Subject'], function ($massage) use ($files){
            $massage->to('test.testing.26.10.1987@gmail.com', 'filesXml.xlsx')->subject('Выгрузка');
            $massage->from('test.testing.26.10.1987@gmail.com',"Вот для вас файл")->attachData($files,'filesXml.xlsx');
        });
        return redirect('admin/files')->with('status', 'The file was sent to the post office!');
    }
}
