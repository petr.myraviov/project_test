<?php

namespace App\Http\Controllers;

use App\Imports\FileImport;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Facades\Excel;
use PHPUnit\TextUI\XmlConfiguration\Logging\TestDox\Xml;

class UploadFilesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $files = File::paginate(5);
        $number = +1;
        return view('admin.load.index', compact('files', 'number'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.load.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $files_xml = new File($request->all());
         ($md5Name = md5_file($request->file('files')->getRealPath()));
        $guessExtension = $request->file('files')->guessExtension();
        $file = $request->file('files')->storeAs('', 'files'.'.'.$guessExtension  ,'public');
            ($files_xml['Fahrzeug'] = $file);

        error_reporting(E_ALL);
        ini_set('display_errors ', 'on');
        (($xml = file_exists('storage/files.xml')) == false);
        ($xml = simplexml_load_file('storage/files.xml'));
                foreach ($xml as $value) {
                    $files_xml = new File($request->all());
                    $files_xml['import'] = intval($value->Import);
                    $files_xml['number'] = intval($value->VCNummer);
                    $files_xml['category'] = intval($value->Kategorie);
                    $files_xml['category_unter'] = intval($value->Unterkategorie);
                    $files_xml['mountain_bike'] = intval($value->Mountainbike);
                    $files_xml['stock'] = intval($value->LagerNr);
                    $files_xml['create_at'] = strval($value->ErfassungsDatum);
                    $files_xml['update_at'] = strval($value->MutationsDatum);
                    $files_xml['title'] = ucfirst($value->Titel);
                    $files_xml['speed'] = intval($value->Geschwindigkeit);
                    $files_xml['brand'] = strtoupper($value->Marke);
                    $files_xml['model'] = strtoupper($value->Modell);
                    $files_xml['suspension'] = intval($value->Federung);
                    $files_xml['frame_style'] = intval($value->Rahmenart);
                    $files_xml['wheel_size'] = intval($value->Radgroesse);
                    $files_xml['frame_material'] = intval($value->Rahmenmaterial);
                    $files_xml['frame_size'] = intval($value->Rahmengroesse);
                    $files_xml['frame_type'] = intval($value->Rahmentyp);
                    $files_xml['frame_size_2'] = intval($value->Rahmengroesse2);
                    $files_xml['drive'] = intval($value->Antrieb);
                    $files_xml['drive_text'] = strtoupper($value->Antriebtext);
                    $files_xml['colour'] = intval($value->Farbe);
                    $files_xml['color_text'] = strtoupper($value->Farbetext);
                    $files_xml['engine'] = intval($value->Motor);
                    $files_xml['engine_info'] = strtoupper($value->Motorinfo);
                    $files_xml['model_year'] = intval($value->Modelljahr);
                    $files_xml['status'] = intval($value->Zustand);
                    $files_xml['warranty_period'] = ucfirst($value->Garantiezeit);
                    $files_xml['selling_price'] = intval($value->Verkpreis);
                    $files_xml['rate_code'] = ucfirst($value->Titel);
                    $files_xml['factory_price'] = intval($value->Neupreis);
                    $files_xml['description'] = strval($value->Beschreibung);
                    $files_xml['photo'] = strtolower($value->Fotolink);
                    $files_xml['photo_2'] = strtolower($value->Fotolink2);
                    $files_xml['photo_3'] = strtolower($value->Fotolink3);
                    $files_xml['title_4'] = strtolower($value->Fotolink4);
                    $files_xml['photo_5'] = strtolower($value->Fotolink5);
                    $files_xml['active'] = intval($value->Aktiv);
                    ($files_xml->save());
        }
                ($xml);
        return redirect()->route('admin.load.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function import()
    {
        (new FileImport)->import('public/c489cb6883c744a21a89ce632546e0a6.xml', null, \Maatwebsite\Excel\Excel::XML);
        return redirect('admin/files')->with('status', 'Data import!');
    }
}
