<?php

namespace App\Http\Controllers;

use App\Http\Requests\TestDataRecordRequest;
use App\Imports\TestDataRecordImport;
use App\Models\TestDataRecord;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class TestDataRecordController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $number = 1;
        $files = TestDataRecord::paginate(5);

        return view('admin.files.index', compact('number', 'files'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.files.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param TestDataRecordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TestDataRecordRequest $request)
    {
        $file = new TestDataRecord($request->all());
        $file->save();
        return redirect()->route('admin.files.index')->with("status", "Create new files {$file->title}!");

    }


    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $file = TestDataRecord::findOrfail($id);
        return view('admin.files.show', compact('file'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $file = TestDataRecord::findOrfail($id);
        return view('admin.files.edit', compact('file'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param TestDataRecordRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TestDataRecordRequest $request, $id)
    {
        $file = TestDataRecord::findOrfail($id);
        $file->update($request->all());
        return redirect()->route('admin.files.index')->with('status', "Edit files {$file->title}");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $file = TestDataRecord::findOrfail($id);
        $file->delete();
        return redirect()->route('admin.files.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function import()
    {
        Excel::import(new TestDataRecordImport, 'public/files.xlsx');

        return redirect('admin/files')->with('status', 'Data import!');
    }
}
