<?php

namespace App\Exports;

use App\Models\File;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class FilesXmlExport implements FromCollection, WithCustomStartCell
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return File::all();
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return 'A1';
    }
}
