<?php

namespace App\Exports;

use App\Models\TestDataRecord;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class TestDataRecordExport implements FromCollection, WithCustomStartCell
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return TestDataRecord::all();
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return 'A1';
    }
}
