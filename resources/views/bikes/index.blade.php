@extends('layouts.app')
@section('content')
    <div class="container mt-4">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Warengruppe</th>
                <th scope="col">Typ</th>
                <th scope="col">Marke</th>
                <th scope="col">Model</th>
                <th scope="col">UVP</th>
                <th scope="col">Geschlecht</th>
                <th scope="col">Farbe</th>
                <th scope="col">Anzahl</th>
                <th scope="col">EAN</th>
                <th scope="col">Grösse</th>
                <th scope="col">Jahr</th>
                <th scope="col">Zustand</th>
                <th scope="col">Aktionspreis</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty(($bikes)))
                @foreach($bikes as $counter => $bike)
                    <tr>
                        <th scope="row">{{$bikes->firstItem() + $counter}}</th>
                        <td>{{$bike->product_group}}</td>
                        <td>{{$bike->type}}</td>
                        <td>{{$bike->brand}}</td>
                        <td>{{$bike->model}}</td>
                        <td>{{$bike->price}}</td>
                        <td>{{$bike->gender}}</td>
                        <td>{{$bike->colour}}</td>
                        <td>{{$bike->number}}</td>
                        <td>{{$bike->EAN}}</td>
                        <td>{{$bike->size}}</td>
                        <td>{{$bike->year}}</td>
                        <td>{{$bike->status}}</td>
                        <td>{{$bike->promotional_price}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <div style="margin-top: 50px" class="col-md-13 offset-md-5 offset-lg-5">
        {{$bikes->withQueryString()->links()}}
    </div>
@endsection
