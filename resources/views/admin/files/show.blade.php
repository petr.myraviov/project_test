@extends('layouts.app')
@section('content')
    <div class="container" style="margin-top: 50px; flex: content-box">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">{{$file->title}}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{$file->created_at}}</h6>
                <p class="card-text">{{$file->description}}</p>
                <a href="{{route('admin.files.edit', ['file' => $file])}}" class="card-link">Edit</a>
                <a href="{{route('admin.files.index')}}" class="card-link">Index</a>
            </div>
        </div>
    </div>
    @endsection
