@extends('layouts.app')
@section('content')
    <div class="shadow-sm mt-4">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Import</th>
                <th scope="col">VCNummer</th>
                <th scope="col">Kategorie</th>
                <th scope="col">Unterkategorie</th>
                <th scope="col">Mountainbike</th>
                <th scope="col">LagerNr</th>
                <th scope="col">ErfassungsDatum</th>
                <th scope="col">MutationsDatum</th>
                <th scope="col">Titel</th>
                <th scope="col">Geschwindigkeit</th>
                <th scope="col">Marke</th>
                <th scope="col">Modell</th>
                <th scope="col">Federung</th>
                <th scope="col">Rahmenart</th>
                <th scope="col">Radgroesse</th>
                <th scope="col">Rahmenmaterial</th>
                <th scope="col">Rahmengroesse</th>
                <th scope="col">Rahmentyp</th>
                <th scope="col">Rahmengroesse2</th>
                <th scope="col">Antrieb</th>
                <th scope="col">Antriebtext</th>
                <th scope="col">Antriebinfo</th>
                <th scope="col">Farbe</th>
                <th scope="col">Farbetext</th>
                <th scope="col">Motor</th>
                <th scope="col">Motorinfo</th>
                <th scope="col">Modelljahr</th>
                <th scope="col">Zustand</th>
                <th scope="col">Garantiezeit</th>
                <th scope="col">Verkpreis</th>
                <th scope="col">Preistyp</th>
                <th scope="col">Neupreis</th>
                <th scope="col">Beschreibung</th>
                <th scope="col">Fotolink</th>
                <th scope="col">Fotolink2</th>
                <th scope="col">Fotolink3</th>
                <th scope="col">Fotolink4</th>
                <th scope="col">Fotolink5</th>
                <th scope="col">Aktiv</th>

            </tr>
            </thead>
            <tbody>
            @foreach($xml->Fahrzeug as $file)
                <tr>
                    <th scope="row">{{$file->Import}}</th>
                    <td>{{$file->VCNummer}}</td>
                    <td>{{$file->Kategorie}}</td>
                    <td>{{$file->Unterkategorie}}</td>
                    <td>{{$file->Mountainbike}}</td>
                    <td>{{$file->LagerNr}}</td>
                    <td>{{$file->ErfassungsDatum}}</td>
                    <td>{{$file->MutationsDatum}}</td>
                    <td>{{$file->Titel}}</td>
                    <td>{{$file->Geschwindigkeit}}</td>
                    <td>{{$file->Marke}}</td>
                    <td>{{$file->Modell}}</td>
                    <td>{{$file->Federung}}</td>
                    <td>{{$file->Rahmenart}}</td>
                    <td>{{$file->Radgroesse}}</td>
                    <td>{{$file->Rahmenmaterial}}</td>
                    <td>{{$file->Rahmengroesse}}</td>
                    <td>{{$file->Rahmentyp}}</td>
                    <td>{{$file->Rahmengroesse2}}</td>
                    <td>{{$file->Antrieb}}</td>
                    <td>{{$file->Antriebtext}}</td>
                    <td>{{$file->Antriebinfo}}</td>
                    <td>{{$file->Farbe}}</td>
                    <td>{{$file->Farbetext}}</td>
                    <td>{{$file->Motor}}</td>
                    <td>{{$file->Motorinfo}}</td>
                    <td>{{$file->Modelljahr}}</td>
                    <td>{{$file->Zustand}}</td>
                    <td>{{$file->Garantiezeit}}</td>
                    <td>{{$file->Verkpreis}}</td>
                    <td>{{$file->Preistyp}}</td>
                    <td>{{$file->Neupreis}}</td>
                    <td>{{$file->Beschreibung}}</td>
                    <td><img src="{{asset('storage/'.$file->Fotolink)}}" alt="">{{$file->Fotolink}}</td>
                    <td>{{$file->Fotolink2}}</td>
                    <td>{{$file->Fotolink3}}</td>
                    <td>{{$file->Fotolink4}}</td>
                    <td>{{$file->Fotolink5}}</td>
                    <td>{{$file->Aktiv}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
