@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="{{route('admin.files.store')}}" method="post">
            @csrf
            @method("post")
            <div class="form-group">
                <label for="exampleFormControlInput1">Title</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror"  name="title" id="exampleFormControlInput1">
                @error('title')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Description</label>
                <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                @error('description')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit">Save</button>
        </form>
        <style>
            .link {
                width:100%;
                display:flex;
                justify-content:flex-end;
            }
        </style>
        <div class="link">
            <a href="{{route('admin.files.index')}}">Index</a>
        </div>
    </div>

@endsection
