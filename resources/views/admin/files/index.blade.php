@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <a href="{{route('admin.files.create')}}">New Create file</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Handle</th>
                <th scope="col">Handle</th>
            </tr>
            </thead>
            <tbody>
            @foreach($files as $file)
                <tr>
                    <th scope="row">{{$number++}}</th>
                    <td>{{$file->title}}</td>
                    <td>{{$file->description}}</td>
                    <td>{{$file->created_at}}</td>
                    <td><a href="{{route('admin.files.show', ['file' => $file])}}">Show</a>|<a href="{{route('admin.files.edit', ['file' => $file])}}">Edit</a>
                        <form action="{{route('admin.files.destroy', ['file' => $file])}}"  method="POST">
                            @csrf
                            @method('delete')
                            <button type="submit">delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div style="margin-top: 50px" class="col-md-13 offset-md-5 offset-lg-5">
        {{$files->withQueryString()->links()}}
    </div>
@endsection
