@extends('layouts.app')
@section('content')
    <div class="shadow-sm mt-4">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Import</th>
                <th scope="col">VCNummer</th>
                <th scope="col">Kategorie</th>
                <th scope="col">Unterkategorie</th>
                <th scope="col">Mountainbike</th>
                <th scope="col">LagerNr</th>
                <th scope="col">ErfassungsDatum</th>
                <th scope="col">MutationsDatum</th>
                <th scope="col">Titel</th>
                <th scope="col">Geschwindigkeit</th>
                <th scope="col">Marke</th>
                <th scope="col">Modell</th>
                <th scope="col">Federung</th>
                <th scope="col">Rahmenart</th>
                <th scope="col">Radgroesse</th>
                <th scope="col">Rahmenmaterial</th>
                <th scope="col">Rahmengroesse</th>
                <th scope="col">Rahmentyp</th>
                <th scope="col">Rahmengroesse2</th>
                <th scope="col">Antrieb</th>
                <th scope="col">Antriebtext</th>
                <th scope="col">Antriebinfo</th>
                <th scope="col">Farbe</th>
                <th scope="col">Farbetext</th>
                <th scope="col">Motor</th>
                <th scope="col">Motorinfo</th>
                <th scope="col">Modelljahr</th>
                <th scope="col">Zustand</th>
                <th scope="col">Garantiezeit</th>
                <th scope="col">Verkpreis</th>
                <th scope="col">Preistyp</th>
                <th scope="col">Neupreis</th>
                <th scope="col">Beschreibung</th>
                <th scope="col">Fotolink</th>
                <th scope="col">Fotolink2</th>
                <th scope="col">Fotolink3</th>
                <th scope="col">Fotolink4</th>
                <th scope="col">Fotolink5</th>
                <th scope="col">Aktiv</th>
                <th scope="col">Warengruppe</th>
                <th scope="col">Typ</th>
                <th scope="col">UVP</th>
                <th scope="col">Geschlecht</th>
                <th scope="col">Anzahl</th>
                <th scope="col">Grösse</th>
                <th scope="col">Jahr</th>
                <th scope="col">Zustand</th>
                <th scope="col">Aktionspreis</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty(($files)))
                @foreach($files as $counter => $file)
                    <tr>
                        <th scope="row">{{$files->firstItem() + $counter}}</th>
                        <td>{{$file->import}}</td>
                        <td>{{$file->number}}</td>
                        <td>{{$file->category}}</td>
                        <td>{{$file->category_unter}}</td>
                        <td>{{$file->mountain_bike}}</td>
                        <td>{{$file->stock}}</td>
                        <td>{{$file->create_at}}</td>
                        <td>{{$file->update_at}}</td>
                        <td>{{$file->title}}</td>
                        <td>{{$file->speed}}</td>
                        <td>{{$file->brand}}</td>
                        <td>{{$file->model}}</td>
                        <td>{{$file->suspension}}</td>
                        <td>{{$file->frame_style}}</td>
                        <td>{{$file->wheel_size}}</td>
                        <td>{{$file->frame_material}}</td>
                        <td>{{$file->frame_size}}</td>
                        <td>{{$file->frame_type}}</td>
                        <td>{{$file->frame_size_2}}</td>
                        <td>{{$file->drive}}</td>
                        <td>{{$file->drive_text}}</td>
                        <td>{{$file->drive_info}}</td>
                        <td>{{$file->colour}}</td>
                        <td>{{$file->color_text}}</td>
                        <td>{{$file->engine}}</td>
                        <td>{{$file->engine_info}}</td>
                        <td>{{$file->model_year}}</td>
                        <td>{{$file->status}}</td>
                        <td>{{$file->warranty_period}}</td>
                        <td>{{$file->selling_price}}</td>
                        <td>{{$file->rate_code}}</td>
                        <td>{{$file->factory_price}}</td>
                        <td>{{$file->description}}</td>
                        <td>{{$file->photo}}</td>
                        <td>{{$file->photo_2}}</td>
                        <td>{{$file->photo_3}}</td>
                        <td>{{$file->title_4}}</td>
                        <td>{{$file->photo_5}}</td>
                        <td>{{$file->active}}</td>
                        <td>{{$file->product_group}}</td>
                        <td>{{$file->type}}</td>
                        <td>{{$file->RRP}}</td>
                        <td>{{$file->gender}}</td>
                        <td>{{$file->quantity}}</td>
                        <td>{{$file->size}}</td>
                        <td>{{$file->year}}</td>
                        <td>{{$file->condition}}</td>
                        <td>{{$file->special_price}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <div style="margin-top: 50px" class="col-md-13 offset-md-5 offset-lg-5">
        {{$files->withQueryString()->links()}}
    </div>
@endsection
