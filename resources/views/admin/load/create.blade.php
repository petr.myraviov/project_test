@extends('layouts.app')
@section('content')
    <div class="container">
        <form method="post" enctype="multipart/form-data" action="{{route('admin.load.store')}}">
            @csrf
            <div class="form-group">
                <label for="exampleFormControlInput1">files</label>
                <input type="file" class="form-control" name="files">
            </div>
            <button type="submit">Save</button>
        </form>
        <style>
            .link {
                width:100%;
                display:flex;
                justify-content:flex-end;
            }
        </style>
        <div class="link">
            <a href="{{route('admin.files.index')}}">Index</a>
        </div>
    </div>

@endsection
