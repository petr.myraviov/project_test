<?php

use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::namespace('')
    ->name('admin.')
    ->prefix('admin')
    ->middleware('auth')
    ->group(function () {
        Route::resource('files', \App\Http\Controllers\TestDataRecordController::class);
        Route::resource('load', \App\Http\Controllers\UploadFilesController::class);
        Route::get('admin/load/import', [\App\Http\Controllers\UploadFilesController::class, 'import'])->name('load.import');
    });
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/files/export', [\App\Http\Controllers\MailController::class, 'export'])->name('files.export');
Route::get('/files/import', [\App\Http\Controllers\TestDataRecordController::class, 'import'])->name('import');

Route::get('/files/send', [\App\Http\Controllers\MailController::class, 'send'])->name('files.send');
Route::get('/files/filesXml', [\App\Http\Controllers\MailController::class, 'fiesXml'])->name('files.filesXml');
Route::fallback(function () {
    return abort(404);
});

Route::get('/bikes/index',[\App\Http\Controllers\BikeController::class, 'index'])->name('bikes.index');



//Google Login
Route::get('login/google', [\App\Http\Controllers\Auth\LoginController::class, 'redirectToGoogle'])->name('login.google');
Route::get('login/google/callback',[\App\Http\Controllers\Auth\LoginController::class, 'handelGoogleCallback']);

//FaceBook Login
Route::get('login/facebook', [\App\Http\Controllers\Auth\LoginController::class, 'redirectToFaceBook'])->name('login.facebook');
Route::get('login/facebook/callback',[\App\Http\Controllers\Auth\LoginController::class, 'handelFaceBookCallback']);
