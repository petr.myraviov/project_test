<?php

namespace Tests\Feature;

use App\Models\TestDataRecord;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TestDataRecordTest extends TestCase
{
    use DatabaseTransactions;
    private $files, $user;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->files = TestDataRecord::factory()->create();
        $this->user = User::factory()->create();
    }
    /**
     * @group files
     */
    public function testIndexSuccessAuth()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('admin.files.index'));
        $response->assertStatus(200);
    }
    /**
     * @group files
     */
    public function testIndexFailAuth()
    {
        $response = $this->get(route('admin.files.index'));
        $response->assertRedirect(route('login'));
    }
    /**
     * @group files
     */
    public function testCreateSuccessAuth()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('admin.files.create'));
        $response->assertStatus(200);
    }

    /**
     * @group files
     */
    public function testCreateFailAuth()
    {

        $response = $this->get(route('admin.files.create'));
        $response->assertRedirect(route('login'));
    }

    /**
     * @group files
     */
    public function testEditFailAuth()
    {
        $response = $this->get(route('admin.files.create'));
        $response->assertRedirect(route('login'));
    }


    /**
     * @group files
     * @return void
     */
    public function testSuccessGetViewIndex()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('admin.files.index'));
        $response->assertStatus(200);
        $response->assertSee('delete');
        $response->assertSee('Show');
        $response->assertSee('Edit');
        $response->assertSee('New Create file');
    }

    /**
     * @group files
     * @return void
     */
    public function testFailedGetViewIndexIfNotAuth()
    {
        $response = $this->get(route('admin.files.index'));
        $response->assertStatus(302);
        $response->assertDontSee('delete');
        $response->assertDontSee('Show');
        $response->assertDontSee('Edit');
        $response->assertDontSee('New Create file');
        $response->assertRedirect(route('login'));
    }


}
