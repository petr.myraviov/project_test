<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group test
     * @return void
     * @throws \Throwable
     */
    public function testSuccessLogin()
    {
        $this->browse(function ($browser) {
            $browser->visit('/login')
                ->type('email', 'admin@gmail.com')
                ->type('password', 'password')
                ->press('Login')
                ->assertPathIs('/home')
                ->clickLink('Files List');

        });
    }
}
