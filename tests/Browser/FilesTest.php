<?php

namespace Tests\Browser;

use App\Models\TestDataRecord;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class FilesTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group files
     * @return void
     * @throws \Throwable
     */
    public function testSuccessLogin()
    {
        $this->browse(function ($browser) {
            $browser->visit('/login')
                ->type('email', 'admin@gmail.com')
                ->type('password', 'password')
                ->press('Login')
                ->assertPathIs('/admin/files')
                ->clickLink('Files List');
        });
    }
    /**
     * @throws \Throwable
     * @group files
     */
    public function testSuccessCreateFiles()
    {
        $this->browse(function ($browser)  {
            $browser->visit('/admin/files')
                ->assertSeeLink('New Create file')
                ->visit('/admin/files/create')
                ->type('title', 'Test Title')
                ->type('description', 'tests description')
                ->press('Save')
                ->visit('/admin/files')
                ->assertSee('Test Title');
        });
    }

    /**
     * @throws \Throwable
     * @group files
     */
    public function testSuccessEditFiles()
    {
        $file = TestDataRecord::factory()->create([
            'title' => 'Test title',
            'description' => 'test descriptions'
        ]);

        $this->browse(function ($browser) use ($file) {
                 $browser->visit("/admin/files/{$file->id}/edit")
                ->type('title', 'test title 2')
                ->type('description', 'tests description')
                ->press('Save')
                ->visit('/admin/files')
                ->assertSee('test title 2');
        });
    }
    /**
     * @throws \Throwable
     * @group files
     */
    public function testSuccessShowFiles()
    {
        $file = TestDataRecord::factory()->create([
            'title' => 'Test title',
            'description' => 'test descriptions',

        ]);

        $this->browse(function ($browser) use ($file) {
            $browser->visit("/admin/files/{$file->id}")
                ->assertSee('Test title')
                ->assertSee('test descriptions');
        });
    }
    /**
     * @throws \Throwable
     * @group files
     */
    public function testSuccessDeleteFiles()
    {
        $file = TestDataRecord::factory()->create([
            'title' => 'Test title',
            'description' => 'test descriptions'
        ]);

        $this->browse(function ($browser) use ($file) {
            $browser->visit("/admin/files/{$file->id}/edit")
                ->type('title', 'test title 2')
                ->type('description', 'tests description')
                ->press('Save')
                ->visit('/admin/files')
                ->assertSee('test title 2')
                ->pause(500)
                ->visit('/admin/files')
                ->assertSee('test title 2')
                ->press('delete');
        });
    }
}
