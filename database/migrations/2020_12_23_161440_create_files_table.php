<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->integer('import')->nullable();
            $table->integer('number')->nullable();
            $table->integer('category')->nullable();
            $table->integer('category_unter')->nullable();
            $table->integer('mountain_bike')->nullable();
            $table->bigInteger('stock')->nullable()->default(null);
            $table->string('create_at')->nullable();
            $table->string('update_at')->nullable();
            $table->string('title')->nullable();
            $table->string('speed')->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('suspension')->nullable();
            $table->string('frame_style')->nullable();
            $table->string('wheel_size')->nullable();
            $table->string('frame_material')->nullable();
            $table->string('frame_size')->nullable();
            $table->string('frame_type')->nullable();
            $table->string('frame_size_2')->nullable();
            $table->string('drive')->nullable();
            $table->string('drive_text')->nullable();
            $table->string('drive_info')->nullable();
            $table->string('colour')->nullable();
            $table->string('color_text')->nullable();
            $table->string('engine')->nullable();
            $table->string('engine_info')->nullable();
            $table->string('model_year')->nullable();
            $table->string('status')->nullable();
            $table->string('warranty_period')->nullable();
            $table->string('selling_price')->nullable();
            $table->string('rate_code')->nullable();
            $table->string('factory_price')->nullable();
            $table->text('description')->nullable();
            $table->string('photo')->nullable();
            $table->string('photo_2')->nullable();
            $table->string('photo_3')->nullable();
            $table->string('title_4')->nullable();
            $table->string('photo_5')->nullable();
            $table->string('active')->nullable();
            $table->string('product_group')->nullable();
            $table->string('type')->nullable();
            $table->string('RRP')->nullable();
            $table->string('gender')->nullable();
            $table->string('quantity')->nullable();
            $table->string('size')->nullable();
            $table->string('year')->nullable();
            $table->string('condition')->nullable();
            $table->string('special_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
