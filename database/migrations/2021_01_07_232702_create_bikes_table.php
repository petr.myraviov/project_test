<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikes', function (Blueprint $table) {
            $table->id();
            $table->string('product_group')->nullable();
            $table->string('type')->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->float('price')->nullable();
            $table->string('gender')->nullable();
            $table->string('colour')->nullable();
            $table->string('number')->nullable();
            $table->bigInteger('EAN')->nullable();
            $table->string('size')->nullable();
            $table->string('year')->nullable();
            $table->string('status')->nullable();
            $table->float('promotional_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bikes');
    }
}
